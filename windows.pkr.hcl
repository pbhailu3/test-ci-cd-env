packer {
  required_plugins {
    docker = {
      version = ">= 1.0.8"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "windows" {
  image = "mcr.microsoft.com/windows/servercore:ltsc2022"
  container_dir = "c:/app"
  windows_container = true
  commit = true
}

build {
  hcp_packer_registry {
    bucket_name = "gitlab-hcl-windows"
  }
  sources = [
    "source.docker.windows",
  ]
}
