packer {
  required_plugins {
    docker = {
      version = ">= 1.0.8"
      source  = "github.com/hashicorp/docker"
    }
  }
}

source "docker" "ubuntu" {
  image  = "ubuntu:jammy"
  commit = true
}

build {
  hcp_packer_registry {
    bucket_name = "gitlab-hcl-ubuntu"
  }

  name = "gha-hcl-ubuntu"
  sources = [
    "source.docker.ubuntu",
  ]
}
