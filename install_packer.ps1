param (
    [string]$TOKEN
)

$REPO = "hashicorp/packer-internal"
$FILE = "packer-internal_1.11.0-internal_windows_amd64.zip"
$GITHUB_API_ENDPOINT = "api.github.com"

try {
    $response = Invoke-RestMethod -Headers @{Authorization = "token $TOKEN"; Accept = "application/vnd.github+json"} -Uri "https://$GITHUB_API_ENDPOINT/repos/$REPO/releases"
    $asset = $response.assets | Where-Object { $_.name -eq $FILE }

    if (-not $asset) {
        Write-Error "ERROR: Asset not found for file $FILE"
        exit 1
    }

    $ASSET_ID = $asset.id
    Write-Output "Downloading asset..."
    Invoke-RestMethod -Headers @{Authorization = "token $TOKEN"; Accept = "application/octet-stream"} -Uri "https://$GITHUB_API_ENDPOINT/repos/$REPO/releases/assets/$ASSET_ID" -OutFile $FILE
    Write-Output "Download completed. Extracting file..."
    Expand-Archive -Path $FILE -DestinationPath . -Force
    Write-Output "Extraction completed."
} catch {
    Write-Error "An error occurred: $_"
    exit 1
}